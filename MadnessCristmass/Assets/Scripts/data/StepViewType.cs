﻿public enum StepViewType
{
    Nope,
    DeadDeer,
    Cave,
    Gift,
    BigTree,
    BigTree_Elf,
    Elf,
    Elf_Dead,
    Bear,
    Bear_Eat
}