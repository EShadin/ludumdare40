﻿public static class StepsLibrary
{
    public static SpecialStep DamageStep(int amount)
    {
        return new DamageStep(amount);
    }

    public static SpecialStep AddResAndDamage(int eat, int ribbon, int damage)
    {
        return new AddResAndDamage(eat, ribbon, damage);
    }
    
    public static SpecialStep AddElfStep(string name, int health, int hungry, int capricious = 0, int eat = 0, int ribbon = 0)
    {
        return new AddElfStep(CreateElfInfo(name, health, hungry, capricious, eat, ribbon));
    }
    
    public static SpecialStep AddElfStepSpec(SpecialStep step, string name,  int health, int hungry, int capricious = 0, int eat = 0, int ribbon = 0)
    {
        return new AddElfWithSpecialStep(CreateElfInfo(name, health, hungry, capricious, eat, ribbon), step);
    }

    public static SpecialStep AddThiefElf(string name, int health, int hungry, int capricious = 0, int eat = 0, int ribbon = 0, int tE = 0, int tR = 0)
    {
        return new AddElfThief(CreateElfInfo(name, health, hungry, capricious, eat, ribbon, 3, tE, tR));
    }

    private static ElfSpecInfo CreateElfInfo(string name, int health, int hungry, int capricious = 0, int eat = 0, int ribbon = 0, int afStep = 0, int tE = 0, int tR = 0)
    {
        ElfSpecInfo info = new ElfSpecInfo();
        info.Health = health;
        info.Hungry = hungry;
        info.Capricious = capricious;

        info.Name = name;

        info.Eat = eat;
        info.Ribbon = ribbon;

        info.AmountEat = tE;
        info.AmountRibbon = tR;

        return info;
    }

    public static SpecialStep AddResource(int eat, int ribbon)
    {
        return new AddResourceStep(eat, ribbon);
    }

    public static SpecialStep BearAttack()
    {
        return new BeatAttacked();
    }

    public static SpecialStep BearEscape()
    {
        return new BearEscape();
    }

    public static SpecialStep OpenGift(int eat, int ribbon)
    {
        return AddResource(eat, ribbon);
    }

    public static SpecialStep OpenGiftBomb(int damage)
    {
        return DamageStep(damage);
    }

    public static SpecialStep AddElfAndDamage(string name, int health, int hungry, int capric, int eat, int ribbo, int damage)
    {
        return new AddEflAndDamage(CreateElfInfo(name, health, hungry, capric, eat, ribbo), damage);
    }
    
    public static SpecialStep AddThiefAndDamage(string name, int health, int hungry, int capricious = 0, int eat = 0, int ribbon = 0, int tE = 0, int tR = 0, int damage = 0)
    {
        return new AddThiefAndDamage(CreateElfInfo(name, health, hungry, capricious, eat, ribbon, 3, tE, tR), damage);
    }

    public static SpecialStep LeftElf()
    {
        return new LeftElf();
    }
}