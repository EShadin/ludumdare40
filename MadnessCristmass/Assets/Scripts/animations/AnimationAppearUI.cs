﻿using UnityEngine;

public class AnimationAppearUI : MonoBehaviour
{
    public RectTransform RectTrnsf;
    public RectTransform Additive;
    public float Speed;
    public float Offset;
    public int Dir;

    private Vector3 _openPosition;
    private Vector3 _hidePosition;

    private Vector3 _velocity;

    private Vector3 _target;

    private void Awake()
    {
        Initialize();
    }

    public void Initialize()
    {
        var y = RectTrnsf.rect.size.y;
        var percent = 0.0f;
        if (Additive != null)
            percent = Additive.rect.size.y * Offset;

        _openPosition = RectTrnsf.anchoredPosition;
        _openPosition.y -= RectTrnsf.rect.size.y + percent;
        _hidePosition = RectTrnsf.anchoredPosition;
        _hidePosition.y += RectTrnsf.rect.size.y + percent;

        RectTrnsf.anchoredPosition = _hidePosition;
        _target = _hidePosition;
    }

    private void Update()
    {
        RectTrnsf.anchoredPosition = Vector3.SmoothDamp(RectTrnsf.anchoredPosition, _target, ref _velocity, Speed);
    }

    public void SetView(bool state)
    {
        gameObject.SetActive(state);
        RectTrnsf.anchoredPosition = _hidePosition;
        _target = _hidePosition;
    }

    public void SetVisible(bool state)
    {
        _target = state ? _openPosition : _hidePosition;
    }
}