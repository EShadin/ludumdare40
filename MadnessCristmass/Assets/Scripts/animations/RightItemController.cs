﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class RightItemController : MonoBehaviour
{
    public Animator Animator;
    public Sprite[] SpriteLibrary;
    public SpriteRenderer SingleSprite;

    public TreeController TreeController;
    public ElfAnimationController ElfController;
    public BeerAnimationController BeerController;
    
    public void SetVisibleRight(bool state)
    {
        Animator.SetBool("Visible", state);
    }
    
    public void ActivateSwitchType(StepViewType type, Action callback = null)
    {
        StartCoroutine(SwitchRightElement(type, callback));
    }

    IEnumerator SwitchRightElement(StepViewType type, Action callback)
    {
        SetVisibleRight(false);
        yield return new WaitForSeconds(0.5f);
        SetType(type);
        SetVisibleRight(true);

        if (callback != null)
            callback();
    }

    public void ActivateWariantWithDelay(StepViewType type, Action callback = null)
    {
        StartCoroutine(StreamActivateWariant(type, callback));
    }

    IEnumerator StreamActivateWariant(StepViewType type, Action callback)
    {
        SetType(type);
        yield return new WaitForSeconds(1f);
        if (callback != null)
            callback();
    }

    public void SetType(StepViewType type)
    {
        DeactivateAllView();

        switch (type)
        {
            case StepViewType.BigTree:
            case StepViewType.BigTree_Elf:
                TreeControl(type);
                break;

            case StepViewType.Bear:
            case StepViewType.Bear_Eat:
                BearControl(type);
                break;

            case StepViewType.Elf:
            case StepViewType.Elf_Dead:
                ElfControl(type);
                break;

            default:
                SetSprite(type);
                break;
        }
    }

    private void TreeControl(StepViewType type)
    {
        TreeController.SetVisible(true);
        if (type == StepViewType.BigTree_Elf)
            TreeController.ActivateFallDown();
    }

    private void BearControl(StepViewType type)
    {
        BeerController.SetVisible(true);
        if (type == StepViewType.Bear_Eat)
            BeerController.ActionThrowElf();
    }

    private void ElfControl(StepViewType type)
    {
        ElfController.SetVisible(true);
        if (type == StepViewType.Elf_Dead)
            ElfController.SetDead(true);
    }

    private void SetSprite(StepViewType type)
    {
        SingleSprite.gameObject.SetActive(true);
        switch (type)
        {
            case StepViewType.Cave:
                SingleSprite.sprite = SpriteLibrary[1];
                break;
            case StepViewType.DeadDeer:
                SingleSprite.sprite = SpriteLibrary[2];
                break;
            case StepViewType.Gift:
                SingleSprite.sprite = SpriteLibrary[Random.Range(3, 6)];
                break;
            default:
                SingleSprite.sprite = SpriteLibrary[0];
                break;
        }
    }

    private void DeactivateAllView()
    {
        SingleSprite.gameObject.SetActive(false);
        TreeController.SetVisible(false);
        ElfController.SetVisible(false);
        BeerController.SetVisible(false);
    }
}
