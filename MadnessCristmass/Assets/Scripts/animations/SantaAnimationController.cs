﻿using UnityEngine;

public class SantaAnimationController : MonoBehaviour
{
    public const string Walk = "IsWalk";
    public const string Dead = "IsDead";

    public Animator Animator;

    public Vector3 SantaStartPos;
    public Vector3 MenuPos;
    
    private void Start()
    {
        SetWalk(false);
        SetDead(false);
    }

    public void SetMenuState(bool state)
    {
        if (state)
        {
            transform.position = MenuPos;
            SetDead(false);
            SetWalk(true);
        }
        else
        {
            transform.position = SantaStartPos;
            SetDead(false);
            SetWalk(false);
        }
    }
    
    public void SetWalk(bool state)
    {
        SetBool(Walk, state);
    }

    public void SetDead(bool state)
    {
        SetBool(Dead, state);
    }

    private void SetBool(string key, bool value)
    {
        Animator.SetBool(key, value);
    }
}
