﻿using UnityEngine;

public class BeerAnimationController : MonoBehaviour
{
    private const string AnimKey = "ThrowElf";
    public Animator Animator;

    public void ActionThrowElf()
    {
        Animator.SetTrigger(AnimKey);
    }

    public void SetVisible(bool state)
    {
        gameObject.SetActive(state);
    }
}
