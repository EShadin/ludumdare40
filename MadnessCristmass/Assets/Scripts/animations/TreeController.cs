﻿using UnityEngine;

public class TreeController : MonoBehaviour
{
    private const string FallDown = "FallDown";

    public Animator Animator;

    public void ActivateFallDown()
    {
        Animator.SetTrigger(FallDown);
    }

    public void SetVisible(bool state)
    {
        gameObject.SetActive(state);
    }
}
