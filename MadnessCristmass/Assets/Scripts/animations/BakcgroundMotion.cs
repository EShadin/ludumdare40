﻿using UnityEngine;

public class BakcgroundMotion : MonoBehaviour
{
    public float SpeedMotion;
    public float OffsetLimit;

    private bool _motion;
    private Transform _backTrnsf;

    private void Start()
    {
        _backTrnsf = transform;
    }

    public void SetMotion(bool state)
    {
        _motion = state;
    }

    private void Update()
    {
        if (!_motion)
            return;

        _backTrnsf.Translate(Vector3.left * Time.deltaTime * SpeedMotion);
        if (_backTrnsf.position.x < OffsetLimit)
            _backTrnsf.position = Vector3.zero;
    }
}
