﻿using UnityEngine;

public class ElfAnimationController : MonoBehaviour
{
    private const string Dead = "IsDead";

    public Animator Animator;

    public void SetDead(bool state)
    {
        Animator.SetBool(Dead, state);
    }

    public void SetVisible(bool state)
    {
        gameObject.SetActive(state);
    }
}