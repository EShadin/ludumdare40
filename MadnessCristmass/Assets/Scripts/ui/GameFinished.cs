﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameFinished : MonoBehaviour
{
    public TextMeshProUGUI Title;
    public TextMeshProUGUI Statistic;

    public Button CloseButton;
    public Button CloseButton2;

    public GameObject InfoPopup;
    public Button InfoCloseButton;

    public event Action PopupClosed = delegate { };
        
    public void Initialize()
    {
        CloseButton.onClick.AddListener(OnCloseWinPopup);
        InfoCloseButton.onClick.AddListener(OnClosePopup);
        CloseButton2.onClick.AddListener(OnClosePopup);
    }
    
    public void SetInfo(bool isAlive, GameStatistic statistic)
    {
        Title.text = isAlive ? "Congratulations" : "Santa is dead!";

        CloseButton.gameObject.SetActive(isAlive);
        CloseButton2.gameObject.SetActive(!isAlive);

        if (isAlive)
        {
            var elves = statistic.AmountSaveElves;

            Statistic.text = string.Format("You saved {0}/10 elves\n", elves);
            if (elves == 0)
            {
                Statistic.text += "\nYou are a pretty selfish Santa! But who cares?\nSanta is alive, so Christmas is rescued!\nLooks like this is the main point.";
            }
            else if (elves > 0 && elves < 6)
            {
                Statistic.text += "\nAt least a few bastard will work for you around the clock.\nNot much but still not bad, isn't it?";
            }
            else if (elves >= 6 && elves < 8)
            {
                Statistic.text += "\nMore than a half of your elves survived this accident!\nThis is a pretty good result!\nThis little bastards will make you a new sleigh in no time.";
            }
            else if (elves == 8 || elves == 9)
            {
                Statistic.text += "\nSo close and so awesome!\nYou lost 1 or 2 elves, almost nothing!\nNow Santa can have a rest while his minions will do all the work!";
            }
            else if (elves == 10)
            {
                Statistic.text += "\nWow! Fantastic!\nYou are an awesome Santa and a perfect manager!\nYou know who should be sacrificed and who should take a chance to become your loyal minion.\nAn awesome job!";
            }
        }
        else
        {
            Statistic.text = "Elves who lost their leader died starving.";
        }
    }

    public void OnCloseWinPopup()
    {
        gameObject.SetActive(false);
        InfoPopup.SetActive(true);
    }

    public void OnClosePopup()
    {
        InfoPopup.SetActive(false);
        gameObject.SetActive(false);
        PopupClosed();
    }
}