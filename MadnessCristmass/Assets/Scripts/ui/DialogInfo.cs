﻿public class DialogInfo
{
    public bool IsChooseElf;
    public int VariantId;

    public StepViewType Type;

    public string MainText;
    public string[] Variants;

    public DialogInfo()
    {
        VariantId = -1;
    }

    public void SetResult(StepResult result)
    {
        if (string.IsNullOrEmpty(MainText))
            MainText = "";

        if (result.Eat != 0)
            MainText += "\n" + GetEatString(result.Eat);
        if (result.Ribbon != 0)
            MainText += "\n" + GetRibbonString(result.Ribbon);

        if (result.Elves.Count > 0)
            foreach (var elf in result.Elves)
                MainText += "\n" + GetStringElf(elf);
    }

    public bool HasInfo()
    {
        return !string.IsNullOrEmpty(MainText);
    }

    private string GetStringElf(ElfInfo info)
    {
        return string.Format("{0} {1}", info.Name, info.State);
    }

    private string GetEatString(int value)
    {
        if (value > 0)
            return string.Format("Вы получили {0} еды", value);
        else
            return string.Format("Вы потеряли {0} еды", value);
    }

    private string GetRibbonString(int value)
    {
        if (value > 0)
            return string.Format("Вы получили {0} лент", value);
        else
            return string.Format("Вы потеряли {0} лент", value);
    }
}