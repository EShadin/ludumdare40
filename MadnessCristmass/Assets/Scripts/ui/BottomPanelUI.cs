﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BottomPanelUI : MonoBehaviour
{
    private const int SizeOfPool = 20;

    public Animator Animator;
    //public AnimationAppearUI Animator;
    public GameObject PrefabEntity;
    public Transform RootContent;

    public event Action<int> AddEat = delegate { };
    public event Action<int> AddRibbon = delegate { };

    private List<EntityItemUI> _entities;

    private void Awake()
    {
        //Animator.Initialize();
        _entities = new List<EntityItemUI>();
        for (int i = 0; i != SizeOfPool; ++i)
            _entities.Add(CreateEntity());
    }

    public void SetView(bool state)
    {
        gameObject.SetActive(state);
        //Animator.SetView(state);
    }

    public void SetVisible(bool state)
    {
        Animator.SetBool("Visible", state);
    }

    private EntityItemUI CreateEntity()
    {
        var gO = Instantiate(PrefabEntity, Vector3.zero, Quaternion.identity, RootContent);
        var uiEntity = gO.GetComponent<EntityItemUI>();

        uiEntity.ActionRibbon += OnAddRibbon;
        uiEntity.ActionEat += OnAddEat;

        return uiEntity;
    }

    public void DisplayEntities(List<Entity> entities)
    {
        ClearEntityUIItems();
        for (int i = 0, im = entities.Count; i != im; ++i)
        {
            _entities[i].SetEntity(i, entities[i]);
            _entities[i].UpdateView();
            _entities[i].SetVisible(true);
        }
    }

    private void ClearEntityUIItems()
    {
        _entities.ForEach((x) => x.SetVisible(false));
    }

    private void OnAddRibbon(int idx)
    {
        AddRibbon(idx);
    }

    private void OnAddEat(int idx)
    {
        AddEat(idx);
    }
}