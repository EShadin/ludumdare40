﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ElfItem : MonoBehaviour
{
    public Button Btn;
    public TextMeshProUGUI Text;

    public Slider Health;
    public Slider Hunger;

    private GameObject _gOb;

    public void SetVisible(bool state)
    {
        _gOb = _gOb ?? gameObject;
        _gOb.SetActive(state);
    }

    public void SetElfInfo(Entity entity)
    {
        Text.text = entity.Name;
        Health.value = (entity.Health / (float)entity.BaseHealth);
        Hunger.value = 1 - (entity.Hunger / (float)entity.BaseHunger);
    }
}