﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EntityItemUI : MonoBehaviour
{
    public TextMeshProUGUI Name;
    public Image Avatar;
    public Slider HealthBar;
    public Slider HungerBar;

    public TextMeshProUGUI Health;
    public TextMeshProUGUI Bellyful;

    public Button AddRibbon;
    public Button AddEat;

    public Sprite[] Avatars;

    public event Action<int> ActionRibbon = delegate { };
    public event Action<int> ActionEat = delegate { };
    
    private GameObject _gO;

    private int _idx;
    private Entity _entity;

    public GameObject GmObj
    {
        get
        {
            _gO = _gO ?? gameObject;
            return _gO;
        }
    }

    public void Start()
    {
        AddRibbon.onClick.AddListener(() =>
        {
            OnAddRibbon();
        });

        AddEat.onClick.AddListener(() =>
        {
            OnAddEat();
        });
    }

    public void SetVisible(bool state)
    {
        GmObj.SetActive(state);
    }

    public void SetEntity(int idx, Entity entity)
    {
        _idx = idx;
        _entity = entity;
    }

    public void UpdateView()
    {
        Name.text = _entity.Name;
        Avatar.sprite = Avatars[_entity.IsSanta ? 0 : 1];

        Health.text = string.Format("Health : {0}/{1}", _entity.Health, _entity.BaseHealth);
        Bellyful.text = string.Format("Bellyful : {0}/{1}", (_entity.BaseHunger - _entity.Hunger), _entity.BaseHunger);

        HealthBar.value = (_entity.Health / (float)_entity.BaseHealth);
        HungerBar.value = 1 - (_entity.Hunger / (float)_entity.BaseHunger);

        AddRibbon.gameObject.SetActive(HealthBar.value < 1);
        AddEat.gameObject.SetActive(HungerBar.value < 1);
    }

    private void OnAddRibbon()
    {
        ActionRibbon(_idx);
    }

    private void OnAddEat()
    {
        ActionEat(_idx);
    }
}