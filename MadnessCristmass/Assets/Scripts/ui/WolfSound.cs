﻿using UnityEngine;

public class WolfSound : MonoBehaviour
{
    public AudioSource Audio;

    public float Delay;
    private float _curr;

    private void Start()
    {
        _curr = Delay;
    }

    private void Update()
    {
        _curr -= Time.deltaTime;
        if (_curr < 0)
        {
            _curr = Delay;
            Audio.Play();
        }
    }
}