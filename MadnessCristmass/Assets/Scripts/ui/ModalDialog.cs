﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ModalDialog : MonoBehaviour
{
    private const int PoolOfElfItems = 20;

    public Animator Animator;

    public TextMeshProUGUI MainText;
    public TextMeshProUGUI[] BtnText;
    public GameObject[] Buttons;
    public Button Close;

    public GameObject PrefabOfElf;
    private List<ElfItem> _elfItems;

    public GameObject PanelOfChose;
    public GameObject PanelOfElves;
    public Transform ContentOfElves;

    public event Action OnHide = delegate { };

    private bool _waitAnimations;
    private int _variantWithElf;

    public event Action CloseDialog = delegate { };
    public event Action UpdateInfomation = delegate { };
    public event Action<StepViewType> ActivateView = delegate { };
    public event Action<StepViewType, StepViewType> ActivateSwitch = delegate { };
    public event Action GameOver = delegate { };

    private bool _notSwitchStep;
    private int _dialogIdx;
    private GVariant _variants;
    private SantaManager _santaManager;

    public void Initialize(SantaManager santaManager)
    {
        _santaManager = santaManager;
        //Animator.Initialize();
        _elfItems = new List<ElfItem>();
        for (int i = 0; i != PoolOfElfItems; ++i)
        {
            int numElf = i;
            var elf = CreateElfItem();
            elf.Btn.onClick.AddListener(() =>
            {
                ActivateVariantOfElf(numElf);
            });

            _elfItems.Add(elf);
        }

        for (int i = 0; i != Buttons.Length; ++i)
        {
            int n = i;
            Buttons[i].GetComponent<Button>().onClick.AddListener(() =>
            {
                ActivateVariant(n);
            });
        }

        Close.onClick.AddListener(CloseButtonClicked);
    }

    public void SetView(bool state)
    {
        gameObject.SetActive(state);
    }

    public void SetVisible(bool state)
    {
        Animator.SetBool("Visible", state);
    }

    public void SetElves(List<Entity> _elves)
    {
        _elfItems.ForEach((x) => x.SetVisible(false));

        for (int i = 0, im = _elves.Count; i != im; ++i)
        {
            _elfItems[i].SetElfInfo(_elves[i]);
            _elfItems[i].SetVisible(true);
        }
    }
    
    public void SelectPanel(int idx)
    {
        PanelOfChose.SetActive(idx == 0);
        PanelOfElves.SetActive(idx == 1);
    }

    private void OnDestroy()
    {
        if (_elfItems != null)
        {
            _elfItems.ForEach((x) => x.Btn.onClick.RemoveAllListeners());
            _elfItems.Clear();
            _elfItems = null;
        }

        for (int i = 0; i != Buttons.Length; ++i)
            Buttons[i].GetComponent<Button>().onClick.RemoveAllListeners();

        Close.onClick.RemoveAllListeners();
    }

    public void SetDialog(StepResult info)
    {
        SelectPanel(0);
        InvisibleButtons();

        _notSwitchStep = true;
        string text = info.Dialog;
        foreach (var elf in info.Elves)
        {
            if (elf.State == ElfState.Dead)
                text += "\n[" + elf.Name + " Dead]";
        }
        MainText.text = text;
        Close.gameObject.SetActive(true);
        SetVisible(true);
    }

    public void SetDialogs(GVariant dialog)
    {
        _dialogIdx = 0;
        _variants = dialog;

        if (_waitAnimations)
            StartCoroutine(WaitAnimations());
        else
            UpdateDialogs();
    }

    private IEnumerator WaitAnimations()
    {
        SetVisible(false);
        yield return new WaitForSeconds(1);
        UpdateDialogs();

        _waitAnimations = false;
    }

    private void UpdateDialogs()
    {
        ActivateView(_variants.Type);
        ActivateVariants();
    }
    
    private void ActivateClosePopup()
    {
        CloseDialog();
        SetVisible(false);
    }

    private bool _isLast;

    private void ActivateVariants()
    {
        SelectPanel(0);
        InvisibleButtons();
        
        MainText.text = "";

        //Debug.Log("Check Empty " + CheckEmptyDialog());
        
        if (_dialogIdx == 0 && _variants.Dialogs.Count == 1)
        {
            Debug.Log("1 "+_variants.Dialogs[0]);
            if (!ActivateSpecialStep())
                return;

            MainText.text = _variants.Dialogs[_dialogIdx++] + (string.IsNullOrEmpty(MainText.text) ? "" : MainText.text);
            
            Close.gameObject.SetActive(_variants.SubVar.Count == 0);

            for (int i = 0, im = _variants.SubVar.Count; i != im; ++i)
            {
                BtnText[i].text = _variants.SubVar[i].Name;
                var isValid = true;
                if (_variants.SubVar[i].Step != null)
                    isValid = _variants.SubVar[i].Step.IsValidStep(_santaManager);
                Buttons[i].gameObject.SetActive(isValid);
            }
        }
        else if (_dialogIdx < _variants.Dialogs.Count - 1)
        {
            Debug.Log("2");
            MainText.text = _variants.Dialogs[_dialogIdx++] + (string.IsNullOrEmpty(MainText.text) ? "" : MainText.text);
            Close.gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("3 "+ _dialogIdx+" " + _variants.Dialogs.Count);
            _isLast = true;

            //if (!ActivateSpecialStep())
            //    return;

            if (_dialogIdx >= _variants.Dialogs.Count)
            {
                ActivateClosePopup();
                return;
            }

            MainText.text = _variants.Dialogs[_dialogIdx++] + (string.IsNullOrEmpty(MainText.text) ? "" : MainText.text);

            Close.gameObject.SetActive(_variants.SubVar.Count == 0);

            for (int i = 0, im = _variants.SubVar.Count; i != im; ++i)
            {
                BtnText[i].text = _variants.SubVar[i].Name;
                var isValid = true;
                if (_variants.SubVar[i].Step != null)
                    isValid = _variants.SubVar[i].Step.IsValidStep(_santaManager);
                Buttons[i].gameObject.SetActive(isValid);
            }
        }
        //else if (CheckEmptyDialog())
        //{
        //    if (!ActivateSpecialStep())
        //        return;
        //}
        
        

        SetVisible(true);
    }
    
    private void ActivateVariantOfElf(int elfId)
    {
        SelectPanel(0);
        _santaManager.SelectElf(elfId);
        SetDialogs(_variants.SubVar[_variantWithElf]);
    }

    private void ActivateVariant(int idx)
    {
        if (_variants.SubVar[idx].ChoseElf)
        {
            _variantWithElf = idx;
            SelectPanel(1);
            SetElves(_santaManager.Elves);
        }
        else
        {
            if (_variants.Type != StepViewType.Nope && _variants.SubVar[idx].Type != StepViewType.Nope)
            {
                _waitAnimations = true;
                ActivateSwitch(_variants.Type, _variants.SubVar[idx].Type);
            }

            SetDialogs(_variants.SubVar[idx]);
        }
    }

    private void CloseButtonClicked()
    {
        if (_notSwitchStep)
        {
            _notSwitchStep = false;
            SetVisible(false);
            OnHide();
        }
        else
        {
            ActivateVariants();
        }
    }

    private bool ActivateSpecialStep()
    {
        if (_variants.Step != null)
        {
            var stepResult = _variants.Step.ActivateStep(_santaManager);
            UpdateInfomation();

            foreach (var elf in stepResult.Elves)
            {
                if (elf.State == ElfState.Dead)
                    MainText.text += "\n" + elf.Name + " Dead";
            }

            if (!_santaManager.Santa.IsAlive)
            {
                GameOver();
                SetVisible(false);
                return false;
            }
        }

        return true;
    }

    private bool CheckEmptyDialog()
    {
        return (_variants.Dialogs.Count == 0);
    }

    private bool CheckDialogs()
    {
        if (_dialogIdx > _variants.Dialogs.Count - 1)
        {
            if (_variants.SubVar.Count == 0)
            {
                SetVisible(false);
                ActivateClosePopup();
                return false;
            }
            else
            {
                --_dialogIdx;
            }
            
        }

        return true;
    }
    
    public void SetText(params string[] args)
    {
        InvisibleButtons();
        MainText.text = args[0];

        int amount = args.Length - 1;
        for (int i = 0; i != amount; ++i)
        {
            BtnText[i].text = args[i + 1];
            Buttons[i].gameObject.SetActive(true);
        }
    }
    
    private void InvisibleButtons()
    {
        foreach (var btn in Buttons)
            btn.gameObject.SetActive(false);
    }

    private ElfItem CreateElfItem()
    {
        var gObject = Instantiate(PrefabOfElf, Vector3.zero, Quaternion.identity, ContentOfElves);
        var elf = gObject.GetComponent<ElfItem>();
        return elf;
    }
}