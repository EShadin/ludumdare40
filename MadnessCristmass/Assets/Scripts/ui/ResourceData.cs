﻿using TMPro;
using UnityEngine;

public class ResourceData : MonoBehaviour
{
    public TextMeshProUGUI Eat;
    public TextMeshProUGUI Ribbon;

    public void SetData(int eat, int ribbon)
    {
        Eat.text = eat.ToString();
        Ribbon.text = ribbon.ToString();
    }
}