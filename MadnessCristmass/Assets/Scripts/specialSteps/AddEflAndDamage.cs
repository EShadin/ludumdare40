﻿public class AddEflAndDamage : AddElfStep
{
    private int _damage;

    public AddEflAndDamage(ElfSpecInfo info, int damage) : base(info)
    {
        _damage = damage;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        gameCore.Santa.AddHealth(-_damage);
        return base.ActivateStep(gameCore);
    }
}