﻿using Random = UnityEngine.Random;

public class CapriciousnessStep : SpecialStep
{
    private const int LimitOfHealth = 2;

    public CapriciousnessStep(Entity entity, int stepDelay) : base(entity, stepDelay)
    {
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        if (_entity.IsHunger && CanActivateCapriciousness())
            return ActivateCapriciousness(gameCore);
        else
            return new StepResult();
    }

    public StepResult ActivateCapriciousness(SantaManager gameCore)
    {
        var step = new StepResult();
        step.Eat = -gameCore.GetEat(1);
        _entity.AddHungry(-2);
        //if (_entity.Capricious <= LeavingLimit && IsLeaving())
        //{
        //    gameCore.Statistics.IncValue(ElfState.Leave, _entity.IsThief);
        //    step.AddELf(_entity.Name, ElfState.Leave);
        //    _entity.AddHealth(-5);
        //}
        //else

        if (step.HasInfo())
            step.AddELf(_entity.Name, ElfState.Nope);
        
        return step;
    }

    private bool IsLeaving()
    {
        return Random.Range(0, 2) == 1;
    }

    private bool CanActivateCapriciousness()
    {
        return (_entity.Health <= LimitOfHealth);
    }
}