﻿using System;

public class AddResAndDamage : SpecialStep
{
    private int _eat;
    private int _ribbon;
    private int _damage;

    public AddResAndDamage(int eat, int ribbon, int damage)
    {
        _eat = eat;
        _ribbon = ribbon;
        _damage = damage;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        UnityEngine.Debug.Log("Do Damage "+ _damage);
        gameCore.Santa.AddHealth(-_damage);
        gameCore.AddEat(_eat);
        gameCore.AddRibbon(_ribbon);

        return new StepResult() { Eat = _eat, Ribbon = _ribbon };
    }
}