﻿using System;

public class RudolfLogic : SpecialStep
{
    public override StepResult ActivateStep(SantaManager gameCore)
    {
        gameCore.Santa.AddHungry(-10);
        foreach (var elf in gameCore.Elves)
            elf.AddHungry(-5);

        return new StepResult();
    }
}