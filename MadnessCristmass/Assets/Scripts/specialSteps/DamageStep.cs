﻿public class DamageStep : SpecialStep
{
    private int _amountDamage;

    public DamageStep(int amountDamage)
    {
        _amountDamage = amountDamage;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        gameCore.Santa.AddHealth(-_amountDamage);
        return new StepResult();
    }
}