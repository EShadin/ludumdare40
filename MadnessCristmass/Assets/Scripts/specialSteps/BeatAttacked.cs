﻿public class BeatAttacked : SpecialStep
{
    public override StepResult ActivateStep(SantaManager gameCore)
    {
        gameCore.Elves.ForEach((x) => x.AddHealth(-2));
        gameCore.Santa.AddHealth(-4);

        var step = new StepResult();

        foreach (var elf in gameCore.Elves)
            if (!elf.IsAlive)
            {
                step.AddELf(elf.Name, ElfState.Dead);
                gameCore.Statistics.IncValue(ElfState.Dead, elf.IsThief);
            }

        gameCore.RemoveDeadElves();

        return step;
    }
}