﻿public class FiggyYuleBloodStep : SpecialStep
{
    public FiggyYuleBloodStep()
    {
        StepDelay = 3;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        var step = new StepResult();

        _entity = gameCore.Elves.Find((x) => x.Name.Equals("Figgy Yule"));

        step.Dialog = "Figgy Yule died of blood loss.";
        step.AddELf("Figgy Yule", ElfState.Dead);
        _entity.AddHealth(-5);

        return step;
    }
}