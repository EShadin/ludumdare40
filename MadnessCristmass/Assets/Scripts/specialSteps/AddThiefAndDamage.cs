﻿public class AddThiefAndDamage : AddElfThief
{
    private int _damage;

    public AddThiefAndDamage(ElfSpecInfo info, int damage) : base(info)
    {
        _damage = damage;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        var step = base.ActivateStep(gameCore);
        _entity.SpecialStep = new StealResources(_entity, _info.AmountEat, _info.AmountRibbon, _damage);
        
        return step;
    }
}