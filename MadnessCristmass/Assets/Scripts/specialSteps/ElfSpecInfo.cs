﻿public struct ElfSpecInfo
{
    public int Health;
    public int Hungry;
    public int Capricious;

    public int Eat;
    public int Ribbon;

    public int AfterStep; //special step for thief
    public int AmountEat;
    public int AmountRibbon;

    public string Name;
}