﻿public class AddElfWithSpecialStep : AddElfStep
{
    private SpecialStep _step;
    
    public AddElfWithSpecialStep(ElfSpecInfo info, SpecialStep specialStep) : base(info)
    {
        _step = specialStep;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        var step = base.ActivateStep(gameCore);
        _entity.SpecialStep = _step;
        return step;
    }
}