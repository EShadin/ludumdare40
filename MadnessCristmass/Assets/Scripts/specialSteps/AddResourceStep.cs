﻿public class AddResourceStep : SpecialStep
{
    public int AmountEat;
    public int AmountRibbon;

    public AddResourceStep()
    {
    }

    public AddResourceStep(int eat, int ribbon)
    {
        AmountEat = eat;
        AmountRibbon = ribbon;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        gameCore.AddEat(AmountEat);
        gameCore.AddRibbon(AmountRibbon);

        return new StepResult() { Eat = AmountEat, Ribbon = AmountRibbon };
    }
}