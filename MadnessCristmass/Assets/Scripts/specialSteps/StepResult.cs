﻿using System.Collections.Generic;

public class StepResult
{
    public int Eat;
    public int Ribbon;
    public List<ElfInfo> Elves;

    public string Dialog;

    public StepResult()
    {
        Elves = new List<ElfInfo>();
    }

    public void AddELf(string name, ElfState state)
    {
        Elves.Add(new ElfInfo() { Name = name, State = state });
    }

    public bool HasInfo()
    {
        return (Eat != 0 || Ribbon != 0 || Elves.Count > 0);
    }

    public void MergeInfo(StepResult step)
    {
        Eat += step.Eat;
        Ribbon += step.Ribbon;
        Elves.AddRange(step.Elves);
        Dialog += "\n" + step.Dialog;
    }

    public override string ToString()
    {
        string str = "";
        str += "Eat : " + Eat + "\n";
        str += "Ribbon : " + Ribbon + "\n";
        foreach (var info in Elves)
            str += " - " + info.Name + " : " + info.State + "\n";

        return str; 
    }
}

public struct ElfInfo
{
    public ElfState State;
    public string Name;
}

public enum ElfState
{
    Nope,
    Add,
    Leave,
    Dead,
    Left,
}