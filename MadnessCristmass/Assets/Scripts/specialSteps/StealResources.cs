﻿
public class StealResources : SpecialStep
{
    public int AmountEat;
    public int AmountRibbon;
    private int _damage;

    public StealResources(Entity entity, int eat, int ribbon, int damage = 0)
    {
        _entity = entity;
        AmountEat = eat;
        AmountRibbon = ribbon;
        StepDelay = 3;
        _damage = damage;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        var step = new StepResult();
        gameCore.Santa.AddHealth(-_damage);
        AmountEat = gameCore.GetEat(AmountEat);
        AmountRibbon = gameCore.GetRibbon(AmountRibbon);
        step.Eat = -AmountEat;
        step.Ribbon = -AmountRibbon;
        step.AddELf(_entity.Name, ElfState.Leave);

        if (_damage > 0)
            step.Dialog = _entity.Name + " sabotaged, you killed him (Santa -2 hp).";
        else
            step.Dialog = _entity.Name + " stole food and ran away (-" + AmountEat + " food).";

        _entity.AddHealth(-5);

        _amoutLeft = 0;

        return step;
    }
}