﻿using UnityEngine;

public class AddElfStep : SpecialStep
{
    private ElfSpecInfo _info;

    public AddElfStep(ElfSpecInfo info)
    {
        _info = info;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        _entity = new Entity(new EntityInfo(5, 5));
        _entity.SetHHInfo(_info.Health, _info.Hungry);
        _entity.Name = _info.Name;

        var step = new StepResult();
        step.Eat = _info.Eat;
        step.Ribbon = _info.Ribbon;
        step.AddELf(_entity.Name, ElfState.Add);
        
        gameCore.AddEat(_info.Eat);
        gameCore.AddElf(_entity);
        gameCore.AddRibbon(_info.Ribbon);

        return step;
    }
}