﻿public class LeftElf : SpecialStep
{
    public override StepResult ActivateStep(SantaManager gameCore)
    {
        gameCore.Statistics.IncValue(ElfState.Left, false);
        return new StepResult();
    }
}