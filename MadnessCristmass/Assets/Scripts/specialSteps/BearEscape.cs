﻿using UnityEngine;

public class BearEscape : SpecialStep
{
    public override bool IsValidStep(SantaManager gameCore)
    {
        return gameCore.Elves.Count > 0;
    }

    public override StepResult ActivateStep(SantaManager gameCore)
    {
        gameCore.SelectedElf.AddHealth(-gameCore.SelectedElf.Health);

        var step = new StepResult();
        step.AddELf(gameCore.SelectedElf.Name, ElfState.Dead);

        gameCore.Statistics.IncValue(ElfState.Dead, gameCore.SelectedElf.IsThief);

        gameCore.RemoveElf(gameCore.SelectedElf);
        gameCore.SelectedElf = null;

        return step;
    }
}