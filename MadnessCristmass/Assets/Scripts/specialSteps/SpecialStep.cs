﻿public abstract class SpecialStep
{
    public int StepDelay;
    protected int _amoutLeft;
    protected Entity _entity;

    public SpecialStep(int stepDelay = 0)
    {
        StepDelay = stepDelay;
        _amoutLeft = 0;
    }

    public SpecialStep(Entity entity, int stepDelay)
    {
        InitializeStep(entity, stepDelay);
    }
    
    public virtual bool IsValidStep(SantaManager gameCore)
    {
        return true;
    }
    
    public void InitializeStep(Entity entity, int stepDelay)
    {
        StepDelay = stepDelay;
        _amoutLeft = 0;
        _entity = entity;
    }
    
    public abstract StepResult ActivateStep(SantaManager gameCore);
    
    public bool CanActivateStep()
    {
        UnityEngine.Debug.Log("can Activate "+StepDelay);
        if (StepDelay == 0)
            return true;

        _amoutLeft++;

        UnityEngine.Debug.Log("can Activate " + StepDelay+" "+_amoutLeft);
        return _amoutLeft == StepDelay;
    }
}