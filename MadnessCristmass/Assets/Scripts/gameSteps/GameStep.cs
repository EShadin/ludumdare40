﻿using System;
using System.Collections.Generic;

[Serializable]
public class GameStep
{
    public int DialogId;
    public StepViewType Type;
    public bool NameFromSantaList;
    public List<GameVariant> Variants;

    public GameStep(int dId, StepViewType type)
    {
        DialogId = dId;
        Type = type;

        Variants = new List<GameVariant>();
    }

    public void AddVariant(GameVariant variant)
    {
        Variants.Add(variant);
    }
}