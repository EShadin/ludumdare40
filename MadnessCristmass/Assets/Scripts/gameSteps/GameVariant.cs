﻿using System;

[Serializable]
public class GameVariant
{
    public int DialogId;
    public int VariantNameId;
    public int GameStepId;
    public bool SpecialDialog;
    public StepViewType TypeUpdate;

    public SpecialStep SpecialStep;

    public GameVariant(int vId = -1, int dId = -1, int gStId = -1, bool sD = false, SpecialStep step = null, StepViewType type = StepViewType.Nope)
    {
        VariantNameId = vId;
        DialogId = dId;
        GameStepId = gStId;
        SpecialDialog = sD;
        TypeUpdate = type;
        SpecialStep = step;
    }

    public bool IsEmptyVariant()
    {
        return (DialogId == -1 && SpecialStep == null && GameStepId == -1);
    }
}