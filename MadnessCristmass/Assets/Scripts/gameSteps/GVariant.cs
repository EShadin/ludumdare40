﻿using System.Collections.Generic;

public class GVariant
{
    public string Name;
    public StepViewType Type;
    public bool ChoseElf;
    public List<string> Dialogs;
    public SpecialStep Step;
    public List<GVariant> SubVar;

    public static GVariant CVariant(string name)
    {
        var v = new GVariant();
        v.Name = name;
        v.Dialogs = new List<string>();
        v.SubVar = new List<GVariant>();

        return v;
    }

    public GVariant ReplaceDialog(string text)
    {
        if (Dialogs.Count > 0)
        {
            Dialogs[0] = text;
            return this;
        }
        else
        {
            return AddDialog(text);
        }
    }

    public GVariant AddDialog(string text)
    {
        Dialogs.Add(text);
        return this;
    }

    public GVariant AddVar(GVariant v)
    {
        SubVar.Add(v);
        return this;
    }

    public GVariant AddVar(string name)
    {
        SubVar.Add(GVariant.CVariant(name));
        return this;
    }

    public GVariant AddStep(SpecialStep step)
    {
        Step = step;
        return this;
    }
}