﻿using System.Collections.Generic;

public class GStep
{
    public StepViewType Type;
    public List<string> Dialog;
    public List<GVariant> Variants;
    
    public GStep()
    {
        Dialog = new List<string>();
        Variants = new List<GVariant>();
    }

    public GStep AddD(string text)
    {
        Dialog.Add(text);
        return this;
    }

    public GStep AddV(GVariant v)
    {
        Variants.Add(v);
        return this;
    }
}