﻿using System.Collections.Generic;
using UnityEngine;

public class GLevel
{
    private List<GVariant> _list;
    private int _idx;

    public int Miles
    { get { return _idx; } }

    public GLevel()
    {
        Initialize();
    }

    public void Initialize()
    {
        _list = new List<GVariant>();
        
        FirstStep();
        SecondStep();
        ThirdStep();
        Bernard();
        Present(2, 0, 0);
        EmptyStep();
        AlabasterSnowball();
        Present(2, 0, 0);
        EmptyStep();
        EmptyStep();
        BushyEvergreen();
        ShinnyUpatree();
        EmptyStep();
        Present(0, 2, 0);
        NoelNougat();
        EmptyStep();
        Present(2, 2, 0);
        PepperMinstix();
        CaveBear();
        Present(5, 4, 0);
        EmptyStep();
        FiggyYule();
        EmptyStep();
        Present(3, 2, 0);
        WunorseOpenslae();
        Rudolf();
        Bear();
        CheerfulCedar();
        Present(0, 0, 0);
        SugarplumMary();
        Present(5, 0, 0);
        EmptyStep();
        Present(0, 0, 0);
        TastyLicorice();
        EmptyStep();
        Present(5, 5, 0);
        Present(0, 0, 4);
        Present(3, 3, 0);
        HeadySyllabub();
        EmptyStep();
        HoneyHanben();
        Present(10, 2, 0);
        EmptyStep();
        CloveEater();
        Present(4, 2, 0);
        CaveGift(10, 2, 0);
        EmptyStep();
        RaspberryPie();
        EmptyStep();
        CaveGift(10, 1, 0);
        EmptyStep();

        ResetLevel();
    }

    public void ResetLevel()
    {
        _idx = 0;
    }

    public GVariant GetStep()
    {
        if (_idx > _list.Count - 1)
            return null;
        else
            return _list[_idx++];
    }

    private void EmptyStep()
    {
        var v = GVariant.CVariant("");
        AddV(v);
    }

    private void FirstStep()
    {
        var v = GVariant.CVariant("");
        v.AddDialog("My sleigh was broken.\nNow I am at the middle of a snow desert and nobody around…");
        v.AddDialog("Fuck! I lost my elves! I need to find them quickly!\nIt will be super hard to find new employees who will work for food.");
        v.AddDialog("Always forget about this little fuckers. How much I had ?\nI should have noticed their names somewhere…");

        AddV(v);
    }

    private void SecondStep()
    {
        var v = GVariant.CVariant("");
        v.AddDialog("Here is my list!\nElves have such shitty names, I even made some notes how to determine them.");
        v.AddDialog("The list is torn! Looks like one of elves chewed it during the flight.");
        AddV(v);
    }

    private void ThirdStep()
    {
        var v = GVariant.CVariant("");
        v.AddDialog("I am around 50 miles away from my factory. It will be a long way, I need to find food to not die from starvation.\nAlso I will need some festive ribbons. Will be useful as bandages");

        AddV(v);
    }

    private void Bernard()
    {
        var v = CreateBaseElf("Bernard", 4, 2);
        v.SubVar[0].AddDialog("Bernard. You always remember me, Santa.");
        v.SubVar[0].SubVar[0].ReplaceDialog("Sure! Elf with a normal name, come with me.");
        v.SubVar[0].SubVar[1].Dialogs[0] = ("Not today.");
        
        AddV(v);
    }

    private void AlabasterSnowball()
    {
        var v = CreateBaseElf("Alabaster Snowball",3, 3, 0, 2);
        v.SubVar[0].AddDialog("Alabaster Snowball! Fastest Elf!");

        AddV(v);
    }

    private void BushyEvergreen()
    {
        var v = GVariant.CVariant("").AddDialog("You found a cave. Wonder what is inside?");
        v.Type = StepViewType.Cave;

        var invistigate = GVariant.CVariant("Investigate.");
        invistigate.Type = StepViewType.Elf;
        invistigate.AddDialog("You found an elf in the cave.");

        var subVName = GVariant.CVariant("Ask his name.");
        subVName.AddDialog("Bushy Evergreen. Do you remember me, Santa?");
        invistigate.AddVar(subVName); //Main : Ask his name.

        var addElf = GVariant.CVariant("Take him with you.");
        addElf.AddDialog("Yeah, longelf");
        addElf.Step = GetElfStep("Bushy Evergreen", 2, 1, 2, 0);
        subVName.AddVar(addElf); //Sub : Take him

        var addElf2 = GVariant.CVariant("Take him with you.");
        addElf2.AddDialog("Yeah, longelf");
        addElf2.Step = GetElfStep("Bushy Evergreen", 2, 1, 2, 0);
        invistigate.AddVar(addElf2); // Main : Take him

        var leaf = GVariant.CVariant("Leave him.");
        leaf.Step = StepsLibrary.LeftElf();
        leaf.AddDialog("One day I will go back for you. Maybe.");
        subVName.AddVar(leaf); // Sub : Leave him.

        var killHim = GVariant.CVariant("Kill him");
        killHim.AddDialog("Sorry, but Santa is hungry.");
        killHim.Step = StepsLibrary.AddResAndDamage(2, 0, 2);
        subVName.AddVar(killHim); // Sub : Kill him

        var leadBif = GVariant.CVariant("Leave him.");
        leadBif.AddDialog("One day I will go back for you. Maybe.");
        invistigate.AddVar("Leave him."); // Main : Leave him

        var subV = GVariant.CVariant("Kill him and rob his body (-2 hp for Santa).");
        subV.AddDialog("Sorry, but Santa is hungry.");
        subV.Step = StepsLibrary.AddResAndDamage(2, 0, 2);
        invistigate.AddVar(subV); // Main : Kill him
        
        v.AddVar(invistigate);
        v.AddVar("Pass it.");

        AddV(v);
    }

    private void PepperMinstix()
    {
        //Tree Elf
        var v = GVariant.CVariant("").AddDialog("You found a really big christmas tree.\nLooks like one elf fell on this tree he is calling for help.");
        v.Type = StepViewType.BigTree;

        var climb = PepperAdditing("Climb the tree to save the elf.", "Pepper Minstix", "Shake him.", 0);
        climb.AddDialog("Elf fell from the tree!");
        v.AddVar(climb);

        var order = GVariant.CVariant("Order him to come down."); /*PepperAdditing("", "Sugarplum Mary!", "Take him down.", 0);*/
        order.AddDialog("Damn, he crashed.");
        order.Type = StepViewType.BigTree_Elf;
        v.AddVar(order);
        v.AddVar("Leave him");
        
        AddV(v);
    }

    private GVariant PepperAdditing(string nameVariant, string name, string varName, int damage)
    {
        var v2 = GVariant.CVariant(nameVariant);

        var v = GVariant.CVariant(varName);
        v.Type = StepViewType.Elf;
        v.Step = StepsLibrary.AddResAndDamage(0, 0, damage);
        v.AddDialog("Here is an elf in front of you.");

        var subVName = GVariant.CVariant("Ask his name.");
        subVName.AddDialog("Pepper Minstix!");
        subVName.AddDialog("Are you sick or something?");
        subVName.AddDialog("No, my skin is green, Santa.");
        v.AddVar(subVName); //Main : Ask his name.

        var addElf = GVariant.CVariant("Take him with you.");
        addElf.AddDialog("Exactly!");
        addElf.Step = GetElfStep(name, 3, 3, 1, 1);
        subVName.AddVar(addElf); //Sub : Take him

        var addElf2 = GVariant.CVariant("Take him with you.");
        addElf2.AddDialog("And the shortest one");
        addElf2.Step = GetElfStep(name, 3, 3, 1, 1);
        v.AddVar(addElf2); // Main : Take him

        var leaf = GVariant.CVariant("Leave him.");
        leaf.Step = StepsLibrary.LeftElf();
        leaf.AddDialog("One day I will go back for you. Maybe.");
        subVName.AddVar(leaf); // Sub : Leave him.

        var killHim = GVariant.CVariant("Kill him");
        killHim.AddDialog("Sorry, but Santa is hungry.");
        killHim.Step = StepsLibrary.AddResAndDamage(1, 1, 2);
        subVName.AddVar(killHim); // Sub : Kill him

        var leadBif = GVariant.CVariant("Leave him.");
        leadBif.AddDialog("One day I will go back for you. Maybe.");
        v.AddVar("Leave him."); // Main : Leave him

        var subV = GVariant.CVariant("Kill him and rob his body (-2 hp for Santa).");
        subV.AddDialog("Sorry, but Santa is hungry.");
        subV.Step = StepsLibrary.AddResAndDamage(1, 1, 2);
        v.AddVar(subV); // Main : Kill him

        v2.AddVar(v);
        return v2;
    }

    private void ShinnyUpatree()
    {
        var v = CreateBaseElf("Shinny Upatree",4, 1, 0, 1, 0, 0, 2);
        v.SubVar[0].AddDialog("Shinny Upatree! I am your favorite elf!");
        v.SubVar[0].AddDialog("Why?");
        v.SubVar[0].AddDialog("Because I am courageous!");
        v.SubVar[0].SubVar[0].ReplaceDialog("I think i remember something like this.\n(behind : He-he)");
        v.SubVar[0].SubVar[1].Dialogs[0] = "Looks like you are lying to Santa. For punishment spend a few days in the snow desert.";
        v.SubVar[0].SubVar[2].Dialogs[0] = "Santa will punish you for your lies, little bastard!";

        AddV(v);
    }
    
    private void SugarplumMary()
    {
        var v = GVariant.CVariant("").AddDialog("You found a really big christmas tree.\nLooks like one elf fell on this tree he is calling for help.");
        v.Type = StepViewType.BigTree;

        var climb = SugarplumAdditing("Climb the tree to save the elf.", "Sugarplum Mary!", "Shake him.", 0);
        climb.AddDialog("Elf fell from the tree!");
        v.AddVar(climb);
        var order = GVariant.CVariant("Order him to come down."); /*PepperAdditing("", "Sugarplum Mary!", "Take him down.", 0);*/
        order.AddDialog("Damn, he crashed.");
        order.Type = StepViewType.BigTree_Elf;
        v.AddVar(order);
        v.AddVar("Leave him");

        AddV(v);
    }

    private GVariant SugarplumAdditing(string nameVariant, string name, string varName, int damage)
    {
        var v2 = GVariant.CVariant(nameVariant);
        v2.Step = StepsLibrary.AddResAndDamage(0, 0, damage);

        var v = GVariant.CVariant(varName);
        v.Type = StepViewType.Elf;
        v.Step = StepsLibrary.AddResAndDamage(0, 0, damage);
        v.AddDialog("Here is an elf in front of you.");

        var subVName = GVariant.CVariant("Ask his name.");
        subVName.AddDialog("Sugarplum Mary!");
        v.AddVar(subVName); //Main : Ask his name.

        var addElf = GVariant.CVariant("Take him with you.");
        addElf.AddDialog("Turn back! Ok, a ponytail.");
        addElf.Step = GetElfStep(name, 3, 1, 2, 2);
        subVName.AddVar(addElf); //Sub : Take him

        var addElf2 = GVariant.CVariant("Take him with you.");
        addElf2.AddDialog("And the shortest one");
        addElf2.Step = GetElfStep(name, 3, 1, 2, 2);
        v.AddVar(addElf2); // Main : Take him

        var leaf = GVariant.CVariant("Leave him.");
        leaf.Step = StepsLibrary.LeftElf();
        leaf.AddDialog("One day I will go back for you. Maybe.");
        subVName.AddVar(leaf); // Sub : Leave him.

        var killHim = GVariant.CVariant("Kill him");
        killHim.AddDialog("Sorry, but Santa is hungry.");
        killHim.Step = StepsLibrary.AddResAndDamage(2, 2, 2);
        subVName.AddVar(killHim); // Sub : Kill him

        var leadBif = GVariant.CVariant("Leave him.");
        leadBif.AddDialog("One day I will go back for you. Maybe.");
        v.AddVar("Leave him."); // Main : Leave him

        var subV = GVariant.CVariant("Kill him and rob his body (-2 hp for Santa).");
        subV.AddDialog("Sorry, but Santa is hungry.");
        subV.Step = StepsLibrary.AddResAndDamage(2, 2, 2);
        v.AddVar(subV); // Main : Kill him

        v2.AddVar(v);
        return v2;
    }

    private void NoelNougat()
    {
        var v = CreateBaseElf("Noel Nougat",2, 2, 2, 0);
        v.SubVar[0].AddDialog("Noel Nougat");
        v.SubVar[0].AddDialog("Can not remember you.");
        v.SubVar[0].AddDialog("You always called me pale skinned.");
        v.SubVar[0].SubVar[0].ReplaceDialog("Ha, you are really pale skinned!");
        v.SubVar[0].SubVar[1].Dialogs[0] = "Sounds not persuasively. One day I will go back for you. Maybe.";

        AddV(v);
    }

    private void CheerfulCedar()
    {
        var v = CreateBaseElf("Cheerful Cedar",3, 2, 0, 2);
        v.SubVar[0].AddDialog("Cheerful Cedar. Any fried cones left for me, Santa?");
        v.SubVar[0].SubVar[0].ReplaceDialog("We are on a diet here.");

        AddV(v);
    }

    private void FiggyYule()
    {
        var v = CreateBaseElf("Figgy Yule", 1, 1, 3, 1); //added special step
        v.SubVar[0].AddDialog("Figgy... Yule…");
        v.SubVar[0].AddDialog("Oh, you looks not very well.");
        v.SubVar[0].AddDialog("I am dying, Santa.");
        v.SubVar[0].SubVar[0].ReplaceDialog("We will try to get you out of here.");
        v.SubVar[0].SubVar[1].Dialogs[0] = ("Sorry, Yule. I can not sacrifice others trying to save you.");
        v.SubVar[0].SubVar[2].Dialogs[0] = ("Rest in peace, my elf. I will stop your torment.");
        v.SubVar[0].SubVar[2].Step = StepsLibrary.AddResource(3, 1);

        v.SubVar[0].SubVar[0].Step = StepsLibrary.AddElfStepSpec(new FiggyYuleBloodStep(), "Figgy Yule", 1, 1, 0, 3, 1);
        v.SubVar[1].Step = v.SubVar[0].SubVar[0].Step;
        v.SubVar[3].Name = "Kill him";
        v.SubVar[3].Step = StepsLibrary.AddResource(3, 1);

        AddV(v);
    }

    private void TastyLicorice()
    {
        var v = CreateBaseElf("Tasty Licorice", 2, 4, 4, 2, 5, 0, 0);
        v.SubVar[0].AddDialog("Tasty Licorice! One of your elves!");
        v.SubVar[0].SubVar[0].ReplaceDialog("Come with us, but I can not remember your strange name.");
        v.SubVar[0].SubVar[1].Dialogs[0] = ("Licorice can not be tasty, liar!");
        v.SubVar[0].SubVar[2].Dialogs[0] = ("Santa will punish you, little impostor.");

        AddV(v);
    }

    private void WunorseOpenslae()
    {
        var v = CreateBaseElf("Wunorse Openslae", 5, 4, 0, 2, 0, 0, 2);
        v.SubVar[0].ReplaceDialog("Wunorse Openslae.");
        v.SubVar[0].AddDialog("Tell me, Wunorse, am I forget something?");
        v.SubVar[0].AddDialog("Wunorse pretty sure about Santa’s good memory!");
        v.SubVar[0].SubVar[0].ReplaceDialog("Ok.");
        v.SubVar[0].SubVar[1].Dialogs[0] = ("You are free now. Search for another job.");
        v.SubVar[0].SubVar[2].Dialogs[0] = ("Before dismissal, Santa will take your food!");
        
        AddV(v);
    }

    private void HoneyHanben()
    {
        var v = CreateBaseElf("Honey Hanben", 3, 2, 0, 2);
        v.SubVar[0].AddDialog("Honey Hanben");
        v.SubVar[0].AddDialog("I can not remember you.");
        v.SubVar[0].AddDialog("Sorry Santa! I am not your elf but I really want to become one!");
        v.SubVar[0].AddDialog("Prove your loyalty.");
        v.SubVar[0].AddDialog("I will work for food!");
        v.SubVar[0].SubVar[0].ReplaceDialog("Good! You passed the interview with one sentence!");
        
        AddV(v);
    }

    private void CloveEater()
    {
        var v = CreateBaseElf("Clove Eater", 2, 5, 0, 0, 4);
        v.SubVar[0].AddDialog("Clove Eater!");
        v.SubVar[0].AddDialog("(One of your elves whispers you that he is a stranger).\nAre you Santa’s elf ? ");
        v.SubVar[0].AddDialog("Sure, I will never lie to Santa!");
        v.SubVar[0].SubVar[0].ReplaceDialog("Ok, I’ll believe you this time.");
        v.SubVar[0].SubVar[1].Dialogs[0] = ("We don’t need “Eaters”, we are starving here!");
        v.SubVar[0].SubVar[2].ReplaceDialog("Santa will punish you, little impostor.");
        AddV(v);
    }

    private void HeadySyllabub()
    {
        var v = GVariant.CVariant("").AddDialog("You found a cave. Wonder what is inside?");
        v.Type = StepViewType.Cave;

        var invistigate = GVariant.CVariant("Investigate.");
        invistigate.Type = StepViewType.Elf;
        invistigate.AddDialog("You found an elf in the cave.");

        var subVName = GVariant.CVariant("Ask his name.");
        subVName.AddDialog("Heady Syllabub!");
        subVName.AddDialog("(One of your elves whispers you that he is a stranger).\nAre you Santa’s elf ?");
        subVName.AddDialog("Sorry Santa! I am not your elf but I really want to become one!");
        subVName.AddDialog("Prove your loyalty.");
        subVName.AddDialog("I will share my resources!");
        invistigate.AddVar(subVName); //Main : Ask his name.

        var addElf = GVariant.CVariant("Take him with you.");
        addElf.AddDialog("Ok, sounds good.");
        addElf.Step = GetElfStep("Heady Syllabub", 4, 1, 5, 5);
        subVName.AddVar(addElf); //Sub : Take him

        var addElf2 = GVariant.CVariant("Take him with you.");
        addElf2.AddDialog("And the shortest one.");
        addElf2.Step = GetElfStep("Heady Syllabub", 4, 1, 5, 5);
        invistigate.AddVar(addElf2); // Main : Take him

        var leaf = GVariant.CVariant("Leave him.");
        leaf.Step = StepsLibrary.LeftElf();
        leaf.AddDialog("One day I will go back for you. Maybe.");
        subVName.AddVar(leaf); // Sub : Leave him.

        var killHim = GVariant.CVariant("Kill him");
        killHim.AddDialog("I will take them from your corpse.");
        killHim.Step = StepsLibrary.AddResAndDamage(5, 5, 2);
        subVName.AddVar(killHim); // Sub : Kill him

        var leadBif = GVariant.CVariant("Leave him.");
        leadBif.AddDialog("One day I will go back for you. Maybe.");
        invistigate.AddVar("Leave him."); // Main : Leave him

        var subV = GVariant.CVariant("Kill him and rob his body (-2 hp for Santa).");
        subV.AddDialog("Sorry, but Santa is hungry.");
        subV.Step = StepsLibrary.AddResAndDamage(5, 5, 2);
        invistigate.AddVar(subV); // Main : Kill him

        v.AddVar(invistigate);
        v.AddVar("Pass it.");

        AddV(v);
    }

    private void RaspberryPie()
    {
        var v = CreateBaseElf("Raspberry Pie", 2, 2, 4, 2);
        v.SubVar[0].AddDialog("Raspberry Pie. Not your elf.");
        v.SubVar[0].AddDialog("What do you want from Santa?");
        v.SubVar[0].AddDialog("Take me with you, I am looking for a job.");
        v.SubVar[0].AddDialog("Food as a salary?");
        v.SubVar[0].AddDialog("Yep.");
        v.SubVar[0].SubVar[0].ReplaceDialog("Ok");

        AddV(v);
    }

    private void Bear()
    {
        var v = GVariant.CVariant("").AddDialog("It is kind of a polar bear. He is very hungry and dangerous.");
        v.Type = StepViewType.Bear;
        
        var fight = GVariant.CVariant("Fight him (-4 health for Santa, -2 for each elf)");
        fight.AddDialog("It was a tough fight, but bear run away like a crying baby! I need use some ribbons to help myself and this little bastards.");
        fight.Step = StepsLibrary.BearAttack();

        var escape = GVariant.CVariant("Feed him with one of your elves and walk away");
        escape.ChoseElf = true;
        escape.AddDialog("You are fired!");
        escape.Type = StepViewType.Bear_Eat;
        escape.Step = StepsLibrary.BearEscape();

        v.AddVar(fight);
        v.AddVar(escape);

        AddV(v);
    }

    private void Present(int eat, int ribbon, int damage)
    {
        var v = GVariant.CVariant("").AddDialog("Do not remember what is inside… Maybe something useful?");
        v.Type = StepViewType.Gift;

        var openIt = GVariant.CVariant("Open it.");
        SettingPresent(openIt, eat, ribbon, damage);

        var passIt = GVariant.CVariant("Pass It.");

        v.AddVar(openIt);
        v.AddVar(passIt);

        AddV(v);
    }

    private void SettingPresent(GVariant openIt, int eat, int ribbon, int damage)
    {
        if (eat == 0 && ribbon == 0 && damage == 0)
            openIt.AddDialog("Some useless stuff for kids.");
        else if (eat > 0 && ribbon == 0 && damage == 0)
            openIt.AddDialog(string.Format("Now this food is for Santa! (+{0} food)", eat));
        else if (eat == 0 && ribbon > 0 && damage == 0)
            openIt.AddDialog(string.Format("Good ribbon. (+{0} festive ribbon)", ribbon));
        else if (eat > 0 && ribbon > 0 && damage == 0)
            openIt.AddDialog(string.Format("Good. \n(+{0} food)\n(+{1} festive ribbon)", eat, ribbon));
        else if (damage > 0)
            openIt.AddDialog("Fuck! I remember this present. Note on it says “for president of any country”.\n(Bomb inside the present detonated - 4 hp to Santa)");

        openIt.Step = StepsLibrary.AddResAndDamage(eat, ribbon, damage);
    }

    private void Rudolf()
    { 
        var v = GVariant.CVariant("").AddDialog("Rudolph is dead. It was a good deer. Maybe he can be useful even after death?");
        v.Type = StepViewType.DeadDeer;

        var earRudolf = GVariant.CVariant("Cook and eat Rudolph’s boody.");
        earRudolf.AddDialog("The festive table is set, my elves! (Everyone have full belly now.)");
        earRudolf.Step = new RudolfLogic();

        var bury = GVariant.CVariant("Bury Rudolph’s boody");
        bury.AddDialog("That was humanly. But I still think it would be tasty.");

        v.AddVar(earRudolf);
        v.AddVar(bury);

        AddV(v);
    }

    private void BigTreeElf(string name, int hp, int hg, int e, int r)
    {
        var v = GVariant.CVariant("").AddDialog("You found a really big christmas tree.\nLooks like one elf fell on this tree he is calling for help.");
        v.Type = StepViewType.BigTree;

        var climb = GVariant.CVariant("Climb the tree to save the elf.");
        climb.Step = StepsLibrary.AddElfAndDamage(name, hp, hg, 0, e, r, 0);
        v.AddVar(climb);

        var order = GVariant.CVariant("Order him to come down.");
        order.Type = StepViewType.BigTree_Elf;
        order.Step = StepsLibrary.AddElfStep(name, hp, hg, 0, e, r);

        v.AddVar(order);

        v.AddVar("Leave him");

        AddV(v);
    }

    private void CaveEmpty()
    {
        var v = GVariant.CVariant("").AddDialog("You found a cave. Wonder what is inside?");
        v.Type = StepViewType.Cave;

        var invistigate = GVariant.CVariant("Investigate.");
        invistigate.AddDialog("Is Empty!");

        v.AddVar(invistigate);
        v.AddVar("Pass it.");

        AddV(v);
    }

    private void CaveBear()
    {
        var v = GVariant.CVariant("").AddDialog("You found a cave. Wonder what is inside?");
        v.Type = StepViewType.Cave;

        var invistigate = GVariant.CVariant("Investigate.");
        invistigate.Type = StepViewType.Bear;
        invistigate.AddDialog("A bear was inside the cave.\nHe is very hungry and dangerous.");

        var fight = GVariant.CVariant("Fight him (-4 health for Santa, -2 for each elf)");
        fight.AddDialog("It was a tough fight, but bear run away like a crying baby! I need use some ribbons to help myself and this little bastards.");
        fight.Step = StepsLibrary.BearAttack();

        var escape = GVariant.CVariant("Feed him with one of your elves and walk away");
        escape.ChoseElf = true;
        escape.AddDialog("You are fired!");
        escape.Type = StepViewType.Bear_Eat;
        escape.Step = StepsLibrary.BearEscape();

        invistigate.AddVar(fight);
        invistigate.AddVar(escape);

        v.AddVar(invistigate);
        v.AddVar("Pass it.");

        AddV(v);
    }

    private void CaveGift(int eat, int ribbon, int damage)
    {
        var v = GVariant.CVariant("").AddDialog("You found a cave. Wonder what is inside?");
        v.Type = StepViewType.Cave;

        var invistigate = GVariant.CVariant("Investigate.");
        invistigate.Type = StepViewType.Gift;
        invistigate.AddDialog("A present was found in the cave.\nDo not remember what is inside… Maybe something useful?");

        var openIt = GVariant.CVariant("Open It.");
        SettingPresent(openIt, eat, ribbon, damage);

        invistigate.AddVar(openIt);
        invistigate.AddVar("Pass it.");

        v.AddVar(invistigate);
        v.AddVar("Pass it.");

        AddV(v);
    }

    private void CaveElf(string name, int hp, int hg, int e, int f)
    {
        var v = GVariant.CVariant("").AddDialog("You found a cave. Wonder what is inside?");
        v.Type = StepViewType.Cave;

        var invistigate = GVariant.CVariant("Investigate.");
        invistigate.Type = StepViewType.Elf;
        invistigate.AddDialog("You found an elf in the cave.");
        invistigate.Step = StepsLibrary.AddElfStep(name, hp, hg, 0, e, f);

        v.AddVar(invistigate);
        v.AddVar("Pass it.");

        AddV(v);
    }

    private GVariant CreateBaseElf(string name, int hp = 0, int hg = 0, int e = 0, int r = 0, int te = 0, int tr = 0, int d = 0)
    {
        var v = GVariant.CVariant("");
        v.Type = StepViewType.Elf;
        v.AddDialog("Here is an elf in front of you.");

        var subVName = GVariant.CVariant("Ask his name.");
        v.AddVar(subVName); //Main : Ask his name.

        var addElf = GVariant.CVariant("Take him with you.");
        addElf.AddDialog("And the shortest one");
        addElf.Step = GetElfStep(name, hp, hg, e, r, te, tr, d);
        subVName.AddVar(addElf); //Sub : Take him

        var addElf2 = GVariant.CVariant("Take him with you.");
        addElf2.AddDialog("And the shortest one");
        addElf2.Step = GetElfStep(name, hp, hg, e, r, te, tr, d);
        v.AddVar(addElf2); // Main : Take him

        var leaf = GVariant.CVariant("Leave him.");
        leaf.Step = StepsLibrary.LeftElf();
        leaf.AddDialog("One day I will go back for you. Maybe.");
        subVName.AddVar(leaf); // Sub : Leave him.

        var killHim = GVariant.CVariant("Kill him");
        killHim.AddDialog("Sorry, but Santa is hungry.");
        killHim.Step = StepsLibrary.AddResAndDamage(e, r, 2);
        subVName.AddVar(killHim); // Sub : Kill him

        var leadBif = GVariant.CVariant("Leave him.");
        leadBif.AddDialog("One day I will go back for you. Maybe.");
        v.AddVar("Leave him."); // Main : Leave him

        var subV = GVariant.CVariant("Kill him and rob his body (-2 hp for Santa).");
        subV.AddDialog("Sorry, but Santa is hungry.");
        subV.Step = StepsLibrary.AddResAndDamage(e, r, 2);
        v.AddVar(subV); // Main : Kill him

        return v;
    }

    private SpecialStep GetElfStep(string name, int hp = 0, int hg = 0, int e = 0, int r = 0, int te = 0, int tr = 0, int d = 0)
    {
        if (te > 0 || tr > 0 || d > 0)
            return StepsLibrary.AddThiefAndDamage(name, hp, hg, 0, e, r, te, tr, d);
        else
            return StepsLibrary.AddElfStep(name, hp, hg, 0, e, r);
    }

    private void AddV(GVariant v)
    {
        _list.Add(v);
    }
}