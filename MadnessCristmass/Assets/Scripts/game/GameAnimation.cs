﻿using System;
using UnityEngine;

public class GameAnimation : MonoBehaviour
{
    public BakcgroundMotion BackgroundMotion;
    public RightItemController RightController;
    public SantaAnimationController SantaAnimations;

    public event Action WalkSantaDone = delegate { };
    public event Action HalfWalkSanta = delegate { };
    public event Action AppearCompleted = delegate { };
    public event Action DisappearCompleted = delegate { };
    public event Action AnimationDone = delegate { };
}