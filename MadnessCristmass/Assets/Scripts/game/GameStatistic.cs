﻿using System.Collections.Generic;

public class GameStatistic
{
    public int AmountSaveElves;
    public int AmountLeavsElves;
    public int AmountKillElves;
    public int AmountLeftElves;

    public void Reset()
    {
        AmountSaveElves = 0;
        AmountLeavsElves = 0;
        AmountKillElves = 0;
        AmountLeftElves = 0;
    }

    public void CalcResult(List<Entity> entities)
    {
        foreach (var elf in entities)
        {
            if (!elf.IsThief)
                AmountSaveElves++;
        }
    }

    public void IncValue(ElfState state, bool isThief)
    {
        switch (state)
        {
            case ElfState.Dead:
                if (!isThief)
                    ++AmountKillElves;
                break;
            case ElfState.Leave:
                if (!isThief)
                    ++AmountLeavsElves;
                break;
            case ElfState.Left:
                AmountLeftElves++;
                break;
        }
    }
}