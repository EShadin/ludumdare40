﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameCore : MonoBehaviour
{
    private SantaManager _santaManager;

    public GameResources GResource
    { get { return _santaManager.GResources; } }

    public GameStatistic Statistics
    { get { return _santaManager.Statistics; } }

    public SantaManager SantaManager
    { get { return _santaManager; } }

    public event Action StepCompletedEvent = delegate { };
    public event Action GameInfoUpdated = delegate { };

    public bool ShouldSwitchSubStep;

    public bool IsSantaAlive
    { get { return _santaManager.Santa.IsAlive; } }
    
    private void Awake()
    {
        _santaManager = new SantaManager();
        _santaManager.SelectedElf = _santaManager.Santa;
    }

    public void RestartGame()
    {
        _santaManager.ResetState();
    }

    private void OnStepCompleted()
    {
        ShouldSwitchSubStep = false;
        StepCompletedEvent();
    }
    
    public void SelectedElf(int elfId)
    {
        _santaManager.SelectElf(elfId);
    }

    public StepResult ActivateSpecialSteps()
    {
        return _santaManager.ActivateSpecialSteps();
    }

    public StepResult NextStepActivate()
    {
        //ShouldSwitchSubStep = true;
        //_stepController.NextStep();
        
        return _santaManager.MakeStep();
    }

    public void GiveEatTo(int idx)
    {
        _santaManager.GiveEatTo(idx);
        GameInfoUpdated();
    }

    public void GiveRibbonTo(int idx)
    {
        _santaManager.GiveRibbonTo(idx);
        GameInfoUpdated();
    }

    public List<Entity> CreateMainEntityList()
    {
        var list = new List<Entity>();
        list.Add(_santaManager.Santa);
        foreach (var elf in _santaManager.Elves)
            list.Add(elf);

        return list;
    }

    public List<Entity> GetElves()
    {
        return _santaManager.Elves;
    }
}