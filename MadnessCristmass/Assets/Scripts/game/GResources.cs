﻿public struct GameResources
{
    public int Eat;
    public int Ribbon;

    public int GetEat(int amount = 1)
    {
        return TryGetValue(ref Eat, amount);
    }

    public int GetRibbon(int amount = 1)
    {
        return TryGetValue(ref Ribbon, amount);
    }
    
    private int TryGetValue(ref int value, int amount)
    {
        if ((value - amount) > 0)
        {
            value -= amount;
            return amount;
        }
        else
        {
            var res = (value - amount) + amount;
            value = 0;
            return res;
        }
    }
}