﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;

public class GameUI : MonoBehaviour
{
    public GameCore GameCore;
    public TextMeshProUGUI MilesIndicator;
    public Button BtnStartGame;
    public Button BtnNextStep;
    public ModalDialog ModelDialog;
    public BottomPanelUI BottomPanel;
    public ResourceData ResourcePanel;
    public Button OpenSantaList;

    public SantaAnimationController SantaAnimations;
    public BakcgroundMotion BackgroundMotion;
    public RightItemController RightItemController;

    public GameFinished FinishedPopup;

    private bool _waitDialog;
    private bool _isCancelStepDone;

    private GLevel _level = new GLevel();

    private void Awake()
    {
        FinishedPopup.Initialize();
        FinishedPopup.PopupClosed += OnCloseFinishPopup;
        
        ModelDialog.CloseDialog += OnClosedDialog;
        ModelDialog.ActivateView += OnActivateViewType;
        ModelDialog.ActivateSwitch += OnActivateSwitch;
        ModelDialog.GameOver += OnGameOver;
        ModelDialog.OnHide += OnHided;
        ModelDialog.UpdateInfomation += UpdateUI;

        BtnStartGame.onClick.AddListener(OnStartNewGame);

        BtnNextStep.onClick.AddListener(() => OnStepBegin());
        GameCore.StepCompletedEvent += OnStepCompleted;
        GameCore.GameInfoUpdated += UpdateUI;

        BottomPanel.AddEat += TryAddEatTo;
        BottomPanel.AddRibbon += TryUseRibbonTo;
    }

    private void Start()
    {
        ModelDialog.Initialize(GameCore.SantaManager);

        BtnStartGame.gameObject.SetActive(true);
        SetGameState(false);
        SantaAnimations.SetMenuState(true);
        //OnCloseFinishPopup();
    }

    private void OnDestroy()
    {
        BtnNextStep.onClick.RemoveAllListeners();
        GameCore.StepCompletedEvent -= OnStepCompleted;
        GameCore.GameInfoUpdated -= UpdateUI;

        BottomPanel.AddEat -= TryAddEatTo;
        BottomPanel.AddRibbon -= TryUseRibbonTo;

        ModelDialog.CloseDialog -= OnClosedDialog;
        ModelDialog.ActivateView -= OnActivateViewType;
        ModelDialog.ActivateSwitch -= OnActivateSwitch;
        ModelDialog.GameOver -= OnGameOver;
        ModelDialog.OnHide -= OnHided;
        ModelDialog.UpdateInfomation -= UpdateUI;

        BtnStartGame.onClick.RemoveAllListeners();
    }

    private void OnCloseFinishPopup()
    {
        //ModelDialog.SetDialogs(_level.GetStep());
        
        BtnStartGame.gameObject.SetActive(true);
        SetGameState(false);
        RightItemController.SetVisibleRight(false);
    }

    private void OnStartNewGame()
    {
        GameCore.RestartGame();
        _level.ResetLevel();

        BtnStartGame.gameObject.SetActive(false);

        //ElvesNamesLibrary.ElfName.MixedNames();

        SetGameState(true);
        BtnNextStep.gameObject.SetActive(false);
        
        OpenSantaList.gameObject.SetActive(false);
        SantaAnimations.SetMenuState(false);

        //_dialogs.Enqueue(GameCore.GetHistoryDialog());
        //ActivateNextDialog();
        UpdateUI();

        ModelDialog.SetDialogs(_level.GetStep());
    }

    public void SetGameState(bool state)
    {
        BottomPanel.SetView(state);
        ModelDialog.SetView(state);

        MilesIndicator.gameObject.SetActive(state);
        BackgroundMotion.SetMotion(!state);
        SantaAnimations.SetMenuState(!state);
    }

    public void UpdateUI()
    {
        MilesIndicator.SetText(string.Format("Miles {0}/{1}", _level.Miles - 1, 50));
        var resource = GameCore.GResource;
        ResourcePanel.SetData(resource.Eat, resource.Ribbon);
        BottomPanel.DisplayEntities(GameCore.CreateMainEntityList());
    }

    private void TryAddEatTo(int uiIdx)
    {
        GameCore.GiveEatTo(uiIdx - 1);
    }

    private void TryUseRibbonTo(int uiIdx)
    {
        GameCore.GiveRibbonTo(uiIdx - 1);
    }

    public void OnStepBegin()
    {
        RightItemController.SetVisibleRight(false);
        BtnNextStep.gameObject.SetActive(false);
        OpenSantaList.gameObject.SetActive(false);
        BottomPanel.SetVisible(false);
        ModelDialog.SetVisible(false); //added

        if (!GameCore.IsSantaAlive)
            return;

        ActivateSpecialStep();


        //_dialogs.Enqueue(GameCore.NextStepActivate());
        //_dialogs.Enqueue(GameCore.GetStepDialog());
        //ActivateNextDialog();
        //UpdateUI();
    }

    private void ActivateSpecialStep()
    {
        var stepResult = GameCore.ActivateSpecialSteps();
        if (stepResult.HasInfo())
        {
            UpdateUI();
            StartCoroutine(AnimationWalk(0.5f, ()=> SpecialStepCallback(stepResult)));
        }
        else
        {
            ActivateMainStepActive(1f);
        }
    }

    private void SpecialStepCallback(StepResult step)
    {
        ModelDialog.SetDialog(step);
        UpdateUI();
        _waitDialog = true;
        StartCoroutine(WaitCloseDialog(() =>
        {
            ActivateMainStepActive(0.5f);
        }));
    }

    private void ActivateMainStepActive(float delay)
    {
        var stepResult = GameCore.NextStepActivate();
        StartCoroutine(AnimationWalk(delay, () => MainStepCallback(stepResult)));
    }

    private void MainStepCallback(StepResult step)
    {
        if (step.HasInfo())
        {
            _waitDialog = true;
            ModelDialog.SetDialog(step);
            StartCoroutine(WaitCloseDialog(ContinueMainStepCallback));
        }
        else
        {
            ContinueMainStepCallback();
        }
    }

    private void ContinueMainStepCallback()
    {
        if (!GameCore.IsSantaAlive)
        {
            OnGameOver();
            return;
        }

        OpenSantaList.gameObject.SetActive(true);
        //BottomPanel.SetVisible(true);
        var nextStep = _level.GetStep();
        if (nextStep != null)
        {
            ModelDialog.SetDialogs(nextStep);
            UpdateUI();
        }
        else
        {
            ActivateFinishPopup();
        }
    }

    private void ActivateFinishPopup()
    {
        GameCore.Statistics.CalcResult(GameCore.GetElves());

        FinishedPopup.SetInfo(GameCore.IsSantaAlive, GameCore.Statistics);
        FinishedPopup.gameObject.SetActive(true);

        BottomPanel.SetView(false);
        ModelDialog.SetView(false);
    }

    IEnumerator WaitCloseDialog(Action nextAction)
    {
        while (_waitDialog)
            yield return null;

        if (nextAction != null)
            nextAction();
    }

    IEnumerator AnimationWalk(float delay, Action callback = null)
    {
        SantaAnimations.SetWalk(true);
        BackgroundMotion.SetMotion(true);
        yield return new WaitForSeconds(delay);

        SantaAnimations.SetWalk(false);
        BackgroundMotion.SetMotion(false);
        if (callback != null)
            callback();
    }

    private void OnHided()
    {
        _waitDialog = false;
    }

    private void OnStepCompleted()
    {
        if (_isCancelStepDone)
        {
            _isCancelStepDone = false;
            return;
        }

        if (GameCore.IsSantaAlive)
        {
            BtnNextStep.gameObject.SetActive(true);
            if (_level.Miles >= 2)
                OpenSantaList.gameObject.SetActive(true);
            BottomPanel.SetVisible(true);
        }
    }
    
    private void OnActivateVariantSub()
    {
        UpdateUI();
    }

    private void OnActivateViewType(StepViewType type)
    {
        if (type != StepViewType.Nope)
        {
            RightItemController.SetType(type);
            RightItemController.SetVisibleRight(true);
        }
    }

    private void OnActivateSwitch(StepViewType typeA, StepViewType typeB)
    {
        if (typeA == StepViewType.Nope && typeB != StepViewType.Nope)
        {
            OnActivateViewType(typeB);
        }
        else if (typeA == StepViewType.Bear && typeB == StepViewType.Bear_Eat ||
            typeA == StepViewType.BigTree && typeB == StepViewType.BigTree_Elf)
        {
            OnActivateViewType(typeB);
        }
        else if (typeA != StepViewType.Nope && typeB != StepViewType.Nope)
        {
            RightItemController.ActivateSwitchType(typeB);
        }

    }

    private void OnGameOver()
    {
        SantaAnimations.SetWalk(false);
        SantaAnimations.SetDead(true);

        ActivateFinishPopup();
    }
    
    private void OnClosedDialog()
    {
        BtnNextStep.gameObject.SetActive(true);
        RightItemController.SetVisibleRight(false);
        BottomPanel.SetVisible(true);
        BtnNextStep.gameObject.SetActive(true);
        //TODO : Activate Next step

        //if (!ActivateNextDialog() && GameCore.ShouldSwitchSubStep)
        //    ActivateSubStep();

        UpdateUI();
    }
}