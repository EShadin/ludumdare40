﻿using System.Linq;
using System.Collections.Generic;

public class SantaManager
{
    public GameResources GResources;
    public Entity Santa;
    public List<Entity> Elves;

    public Entity SelectedElf;

    public GameStatistic Statistics;

    public SantaManager()
    {
        Statistics = new GameStatistic();

        GResources = new GameResources();
        GResources.Eat = 0;
        GResources.Ribbon = 2;

        Santa = new Entity(10, 10);
        Santa.SetHHInfo(7, 5);
        //Santa.AddHungry(10);
        Santa.Name = "Santa";
        Santa.IsSanta = true;

        Elves = new List<Entity>();
    }

    public void ResetState()
    {
        GResources.Eat = 0;
        GResources.Ribbon = 2;

        Santa.SetHHInfo(7, 5);

        Elves.Clear();

        Statistics.Reset();
    }

    public StepResult ActivateSpecialSteps()
    {
        var step = new StepResult();
        foreach (var elf in Elves)
            step.MergeInfo(elf.ActivateSpecialStep(this));

        RemoveDeadElves();

        return step;
    }

    public StepResult MakeStep()
    {
        StepResult resultOfStep = Santa.MakeStep(this);
        var results = Elves.Select((x) => x.MakeStep(this)).Where((x)=>x.HasInfo());
        
        foreach (var step in results)
            resultOfStep.MergeInfo(step);

        RemoveDeadElves();

        return resultOfStep;
    }

    public void RemoveDeadElves()
    {
        Elves = Elves.Where((x) => x.IsAlive).ToList();
    }

    public void SelectElf(int idx)
    {
        SelectedElf = Elves[idx];
    }

    public void GiveEatTo(int idx)
    {
        if (idx == -1)
            Santa.AddHungry(-(GetEat() * 2));
        else
            Elves[idx].AddHungry(-(GetEat() * 2));
    }

    public void GiveRibbonTo(int idx)
    {
        if (idx == -1)
            Santa.AddHealth(GetRibbon() * 2);
        else
            Elves[idx].AddHealth(GetRibbon() * 2);
    }

    public void AddElf(Entity entity)
    {
        if (entity == null)
            return;

        Elves.Add(entity);
    }

    public void RemoveElf(Entity entity)
    {
        if (entity == null)
            return;

        Elves.Remove(entity);
    }

    public void AddEat(int amount)
    {
        GResources.Eat += amount;
    }

    public void AddRibbon(int amount)
    {
        GResources.Ribbon += amount;
    }

    public int GetEat(int amount = 1)
    {
        return GResources.GetEat(amount);
    }

    public int GetRibbon(int amount = 1)
    {
        return GResources.GetRibbon(amount);
    }
}