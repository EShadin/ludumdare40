﻿using Mathf = UnityEngine.Mathf;

public class Entity
{
    protected EntityInfo _currentInfo;

    public bool IsSanta;
    public bool IsThief;

    public string Name;
    public SpecialStep SpecialStep;
    
    public Entity(int health, int hunger)
    {
        _currentInfo = new EntityInfo(health, hunger);
    }

    public Entity(EntityInfo info)
    {
        _currentInfo = info;
    }

    public bool IsAlive
    { get { return Health > 0; } }

    public bool IsHunger
    { get { return Hunger == BaseHunger; } }
    
    public int Health
    { get { return _currentInfo.CurrHealth; } }

    public int Hunger
    { get { return _currentInfo.CurrHunger; } }

    public int BaseHealth
    { get { return _currentInfo.Health; } }

    public int BaseHunger
    { get { return _currentInfo.Hunger; } }
    
    public void AddHealth(int value)
    {
        _currentInfo.CurrHealth += value;
        AddValue(ref _currentInfo.CurrHealth, 0, _currentInfo.Health);
    }

    public void AddHungry(int value)
    {
        _currentInfo.CurrHunger += value;
        AddValue(ref _currentInfo.CurrHunger, 0, _currentInfo.Hunger);
    }

    public void SetHHInfo(int health, int hunger)
    {
        _currentInfo.CurrHealth = health;
        _currentInfo.CurrHunger = hunger;
    }

    private void AddValue(ref int v, int min, int max)
    {
        v = Mathf.Clamp(v, min, max);
    }

    public StepResult MakeStep(SantaManager gameCore)
    {
        var step = new StepResult();

        if (IsHunger)
            AddHealth(-1);

        AddHungry(1);

        if (!IsAlive)
        {
            step.AddELf(Name, ElfState.Dead);
            gameCore.Statistics.IncValue(ElfState.Dead, IsThief);
        }

        return step;
    }

    public StepResult ActivateSpecialStep(SantaManager gameCore)
    {
        if (SpecialStep != null && SpecialStep.CanActivateStep())
            return SpecialStep.ActivateStep(gameCore);
        else
            return new StepResult();
    }
}