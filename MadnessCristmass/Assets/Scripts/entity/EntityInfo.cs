﻿public struct EntityInfo
{
    public int Health;
    public int Hunger;

    public int CurrHealth;
    public int CurrHunger;
    
    public EntityInfo(int health, int hunger)
    {
        Health = health;
        Hunger = hunger;

        CurrHealth = Health;
        CurrHunger = 0;
    }

    public EntityInfo(EntityInfo info)
    {
        Health = info.Health;
        Hunger = info.Hunger;
        
        CurrHealth = Health;
        CurrHunger = 0;
    }
}